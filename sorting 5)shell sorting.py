def shellsort(number):         #define function if number[54,26,93,17,77,31,44,55,20]
    gap=len(number)//2          #9//2=4
    while gap>0:                #4>0
        for i in range(gap,len(number)):     #gap=4 len(number=4,5,6,7,8) now len number=5
            current_number=number[i]         #current_number=77           now 31
            pos=i                            #pos=4
            while pos>=gap and current_number<number[pos-gap]:#pos=4,gap=4 and current_number=77
                number[pos]=number[pos-gap]     #and [pos=4 ,gap=4 4-4=0 ,number[0]=54<77]=false
                pos=pos-gap                                     #
                number[pos]= current_number         #77 placed 
        gap=gap//2                                  #reduce the gap
num=list(map(int,input().strip().split(",")))
shellsort(num)
print(num)                                            
