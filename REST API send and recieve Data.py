from flask import Flask, jsonify

app = Flask(__name__)

cars = [{"brand": "Ford",
         "model": "Mustang",
         "year": "1964"},
        {
            "brand": "suzuki",
            "model": "alto",
            "year": "2000"},
        {
            "brand": "maruti",
            "model": "eight hundred",
            "year": "1996"},
        {
            "brand": "Marutisuzhki",
            "model": "zen",
            "year": "2007"}
        ]


@app.route('/')
def index():
    return 'Welcome Kalimuthu '


@app.route("/cars", methods=['GET'])
def get():
    return jsonify({'Cars': cars})


@app.route("/cars/<int:model>", methods=['GET'])
def get_cars(model):
    return jsonify({'cars': cars[model]})

#create method
@app.route("/cars", methods=['POST'])
def create():
    car = {"brand": "TATA",
           "model": "NEXON",
           "year": "2010"}
    cars.append(car)
    return jsonify({'Created': cars})


#put method
@app.route("/cars/<int:model>", methods=['PUT'])
def car_update(model):
    cars[model]['year']="kkk"
    return jsonify({'car':cars[model]})

#delete method
@app.route("/cars/<int:model>", methods=['DELETE'])
def delete(model):
    cars.remove(cars[model])
    return jsonify({'result':True})

if __name__ == '__main__':
    app.debug = True
    app.run()
#curl -i -H "Content Type:Application/json" -X [PUT,POST,PUT,DELETE]http://localhost:50000/cars
