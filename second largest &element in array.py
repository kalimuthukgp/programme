def find_len(arr1):                         #define function if arr1= [39 42 27 50 31 20 14 56 22]
	length = len(arr1)                                    #length=9
	arr1.sort()                                   #asscending order[14 20 22 27 31 39 42 50 56]
	print("Second Largest element is:", arr1[length-2])    #arr[len-2]=50
	print("Second Smallest element is:", arr1[1])           #arr1 [1]=20
arr1=list(map(int,input().strip().split()))
Largest = find_len(arr1)                                    #function call
