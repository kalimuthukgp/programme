#find first greatest number
k=int(input("Enter the 1st number: "))   #if k=88
s=int(input("Enter the 2nd number: "))   #if s=8
r=int(input("Enter the 3rd number: "))   #if r=888
if(k>=s) and (k>=r):                     #false
    greatest=k
elif(s>=k) and (s>=r):                   #false
    greatest=s
else:
    greatest=r                #r=true, so greatest=r  
print("The greatest number is",greatest)

#after find lowest number
if(k<=s) and (k<=r):            #false
    lowest=k                 
elif(s<=k) and (s<=r):          #true,so lowest=so
    lowest=s
else:
    lowest=r
print("The lowest numberis ",lowest)

#another method
k=int(input("Enter the 1st number: "))
s=int(input("Enter the 2nd number: "))
r=int(input("Enter the 3rd number: "))
print(max(k,s,r),"is greatest")
print(min(k,s,r),"is lowest")
