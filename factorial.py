N=int(input())       
                               #if n=11
t=1                         
for i in range(1,N+1):         #now i value=1 to N+1 
    t=t*i                    #t=1 after i=2 2*1=2 now i=3 3*2=6 now i=4 4*6=24 now i=5 5*24=120
print(t)                     #now i=6 6*120=720 now i=7 7*720=5040 i=8 8*5040=40320    
                             #i=9   9*40320=362880 i=10 10*362880=3628800 i=11 11*3628800=39916800
