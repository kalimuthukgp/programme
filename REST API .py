from http.server import HTTPServer, BaseHTTPRequestHandler
import json

#HERE WE USING LOCAL VARIABLE FOR CRUD OPERATION
#DATA MANIPULATION IS NOT PERSISTANT
#ONCE SERVER IS RESTARTED ALL DATA CHANGES ARE GONE
#GET,GET BY MODEL NAME, PUT(UPDATE), POST(CREATE), DELETE ARE DONE HERE 

allcars=[
        {
         "brand": "Ford",
         "model": "Mustang",
         "year": "1964"
        },
        {
            "brand": "suzuki",
            "model": "alto",
            "year": "2000"
        },
        {
            "brand": "maruti",
            "model": "eight hundred",
            "year": "1996"
        },
        {
            "brand": "Marutisuzhki",
            "model": "zen",
            "year": "2007"
        }
]


class echoHandler(BaseHTTPRequestHandler):

    #GET method
    def do_GET(self):
        a=self.path.split("/")
        if self.path=="/allcars":
            self.send_response(200)
            self.send_header('Content-type', 'text/json')
            self.end_headers()
            self.wfile.write(json.dumps(allcars).encode())
        elif self.path=="/":
            self.send_response(200)
            self.send_header('Content-type', 'text/HTML')
            self.end_headers()
            self.wfile.write("HELLO KALIMUTHU".encode())
        elif len(a)==3 and a[1]=="cars" :
            self.send_response(200)
            self.send_header('Content-type', 'text/json')
            self.end_headers()
            b=False;
            car=[]
            for c in allcars:
                if(c["model"]==a[2]):
                    b=True
                    car=c
                    break
            if(b):
                self.wfile.write(json.dumps(car).encode())
            else:
                self.wfile.write(json.dumps({'message':"no car found"}).encode())

    #POST
    def do_POST(self):
        if self.path=="/addcar":
            self.send_response(200)
            self.send_header('Content-type', 'text/json')
            length = int(self.headers['Content-Length'])
            content = json.loads(self.rfile.read(length))
            self.end_headers()
            b=True
            for c in allcars:
                if(c["model"]==content["model"]):
                    b=False
                    self.wfile.write(json.dumps({'message':"Car model name already exits"}).encode())
                    break
            if(b):
                allcars.append(content)
                self.wfile.write(json.dumps(allcars).encode())

  #PUT(UPDATE)
    def do_PUT(self):
        if self.path=="/updatecar":
            self.send_response(200)
            self.send_header('Content-type', 'text/json')
            length = int(self.headers['Content-Length'])
            content = json.loads(self.rfile.read(length))
            self.end_headers()
            i=-1
            b=True
            l=int(len(allcars))
            for car in allcars:
                i+=1
                if(car["model"]==content["model"]):
                    allcars[i]["brand"]=content["brand"]
                    allcars[i]["year"]=content["year"]
                    self.wfile.write(json.dumps(allcars).encode())
                    b=False
                    break
            if(b):
                self.wfile.write(json.dumps({'msg':"No cars in model named "+content["model"]}).encode())

    #DELETE
    def do_DELETE(self):
        a = self.path.split("/")
        if len(a)==3 and a[1]=="deletecar":
            self.send_response(200)
            self.send_header('Content-type', 'text/json')
            self.end_headers()
            b=True
            for car in allcars:
                if (car["model"] == a[2]):
                    allcars.remove(car)
                    b=False
                    self.wfile.write(json.dumps(allcars).encode())
                    break
            if(b):
                self.wfile.write(json.dumps({'msg':"No cars in model named "+a[2]}).encode())


def main():
    PORT=8080
    server=HTTPServer(('',PORT),echoHandler)
    print('Server running port %s' % PORT)
    server.serve_forever()

if __name__ == '__main__':
    main()
