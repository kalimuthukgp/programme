def insertionsort(number):          #if number=10,4,25,1,5
	for i in range(1,len(number)):        #(1,5)because 0th pos sorted so starts with 1 pos to len
		current_number=number[i]            #current_number=number(i)=4,25,1,5
		pos=i                               #pos=1
		while current_number<number[pos-1] and pos>0: #4<10 and 1>0
		    number[pos]=number[pos-1]                #greatest value right position now 4, 10
		    pos=pos-1                               
		number[pos]= current_number                    #condition false the number same position
num=list(map(int,input().strip().split(",")))
insertionsort(num)
print(num)                                             #output will be [1,4,5,10,25]


