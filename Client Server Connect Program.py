#server program
import socket
def server_program():
    host = socket.gethostname()    # get the hostname
    port = 5000                    # initiate port no above 1024

    server_socket = socket.socket()                  # get instance
    server_socket.bind(('192.168.43.173', port))     # bind host address and port together
    server_socket.listen(2)
    conn, address = server_socket.accept()           # accept new connection
    print("Connection from: " + str(address))
    while True:
        data = conn.recv(1024).decode()
        if not data:                                 # if data is not received break
            break
        print("from connected user: " + str(data))
        data = input(' -> ')
        conn.send(data.encode())                    # send data to the client

    conn.close()                                    # close the connection
if __name__ == '__main__':
    server_program()


#client program
import socket
def client_program():
    host = socket.gethostname()           
    port = 5000                                          # socket server port number

    client_socket = socket.socket()                      # instantiate
    client_socket.connect((host, port))                   # connect to the server

    message = input(" -> ")                               # take input

    while message.lower().strip() != 'bye':
        client_socket.send(message.encode())               # send message
        data = client_socket.recv(1024).decode()           # receive response

        print('Received from server: ' + data)          

        message = input(" -> ")                               # take input

    client_socket.close()                                     # close the connection
if __name__ == '__main__':
    client_program()
