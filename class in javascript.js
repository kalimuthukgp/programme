class animal{
  constructor(name,age){
    console.log(`${name} was animal and class is created`);
    this.name=name;
    this.age=age;
  }
  eat(){
    console.log(`${this.name}is eating`);
  }
  sleep(){
    console.log(`${this.name}is sleeping`);
 
  }
  
}
const jimmy=new animal("jimmy",6);
jimmy.eat();
jimmy.sleep();
console.log("___________________________________________________________");
const rosy=new animal("rosy",6);
rosy.eat();
rosy.sleep();
