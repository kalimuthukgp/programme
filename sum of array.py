k=list(map(int,input().strip().split()) #get array value input  if k=[24,47,11,67,21,98,3]
sum=0                                   #intialize sum=0
for i in k:                             #now k value assigned to i i=24    i=47     i=11    
    sum+=i                                 #sum=0+24=24                    24+47=71  71+11=82 etc
print(sum)                                 
                                       #finally sum of array value is 271
