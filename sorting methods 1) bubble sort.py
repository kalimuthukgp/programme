def sort(number):                         #intialize the function
    for i in range(len(number)-1,0,-1):  # i in range 6 but(-1 because index starts 0 so -1,)
        for j in range(i):                 #starts0 th position,-1 last element,now j=i 
            if number[j]>number[j+1]:     #if number[j]=[9,8,7,6,5,3] now j=9 ,j+1=8 condtion true
                temp=number[j]                               #temp=9          temp=9
                number[j]=number[j+1]                       #number[j]=8    number[j]=7
                number[j+1]=temp                            #number[j+1]=9  #number[j+1]=9
number=list(map(int,input().strip().split(",")))
sort(number)                                              #now[8,9,7,6,5,3]   now[8,7,9,6,5,3]    
print(number)                                               #output will be [3, 5, 6, 7, 8, 9]
