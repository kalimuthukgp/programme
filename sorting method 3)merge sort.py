def mergesort(number):            #if number=8,7,6,5,55,4,1,0
    if len(number)>1:             #condition true 8>1
        mid=len(number)//2         #mid=8/2=4
        left=number[:mid]          #8,7,6,5
        right=number[mid:]          #55,4,1,0
        mergesort(left)
        mergesort(right)
        i=0                         #intialize i  variable
        j=0                         #intialize  j variable
        k=0                         #intialize k variable
        while i<len(left) and j<len(right): #i<8,7,6,5 and j<55,4,1,0
            if left[i] < right[j]:          #8 <7  #6<5 ......
                number[k]=left[i]           
                i+=1
            else:
                number[k]=right[j]      #number[0]=7
                j+=1
            k+=1
        while i<len(left):
            number[k]=left[i]
            i+=1
            k+=1
        while j<len(right):
            number[k]=right[j]
            j+=1
            k+=1
        
number=list(map(int,input().strip().split(",")))
mergesort(number)
print(number)
