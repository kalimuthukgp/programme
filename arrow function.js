console.log("Arrow function");
const hello=() => {
  let es6="ES6";
  return `hello ${es6}`;
};
const powers=[1,2,3,4,5].map((number,index) =>Math.pow(number,index));

let addition=(n1,n2) => n1+n2;
console.log(hello());
console.log(powers);
console.log(addition(100,306));
