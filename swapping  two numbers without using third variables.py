#build in method using
k=int(input("Enter the 1St number : "))
s=int(input("Enter the 2nd number:  "))
print("Before swapping :" ,"k=",k,"s=",s)
k,s=s,k
print("Before swapping :" ,"k=",k,"s=",s)

#using + and - operator for swapping method without third variable

x=int(input("Enter the x value: "))        #if input x=100
y=int(input("Enter the y value: "))        #if input y=120
x=x+y                                      #x=220
y=x-y                                      #y=100
x=x-y                                      #x=120
print("After swapping x value is : ",x)     #now x value is 120
print("After swapping y value is : ",y)     #now y valueis 100
