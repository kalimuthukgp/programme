k=int(input("Enter the number: "))           #k=123
sum=0                                          # itial value sum=0
while(k>0):                                     #(123>0 so true
    a=k%10                                      #a=123%10=3        a=2      a=1 
    sum=sum+a                                   #sum=0+3=3          3+2=5   3+2+1=6
    k=k//10                                     #k=123//10=12       12//10=1 
print(sum)                                      #now print sum of value =6
